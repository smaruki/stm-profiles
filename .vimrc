"General
syntax on                       "Syntax highlight
set showmatch                   "Highlight matching brace ( { [ quando fechados
set nowrap                      "No wrap (quebra de linha)
set mouse=a                     "Enable Mouse Actions
set number                      "Show line numbers
set textwidth=100               "Line wrap (number of cols)
set visualbell                  "Use visual bell (no beeping)
 
set hlsearch                    "Highlight all search results
set smartcase                   "Enable smart-case search
 
set shiftwidth=4                "Number of auto-indent spaces
set smartindent                 "Enable smart-indent
set smarttab                    "Enable smart-tabs
set softtabstop=4               "Number of spaces per Tab
set ts=4                        "Seta onde o tab para
set sw=4                        "largura do tab
set et                          "Espacos em vez de tab

set ruler                       "Show row and column ruler information
 
set undolevels=1000             "Number of undo levels
set backspace=indent,eol,start  "Backspace behaviour
